﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class GameManager : MonoBehaviour 
{
    public static bool isGameOver;

    public Text scoreText;
    public Text highScoreText;
    public GameObject playerObject;
    public GameObject menuButtons;
    public Button pauseButton;
    public Animator menuAnimator;

    private float gameScore;
    private int highScore;
    private static bool isFirstPlay;
    private bool isPaused;

    void Awake()
    {
        menuButtons.SetActive(false);
    }

	// Use this for initialization
	void Start () 
    {
        isPaused = false;

        SetPlayerPrefs();

        
        isGameOver = false;
	}

    void SetPlayerPrefs()
    {
        if (IsFirstPlay())
        {
            //Time.timeScale = 0;

            //TutorialManager.StartTutorial();
        }

        if(PlayerPrefs.HasKey("HighScore"))
        {
            highScore = PlayerPrefs.GetInt("HighScore");
        }
    }
	
	// Update is called once per frame
	void Update () 
    {
        // SCORE UI
        scoreText.text = "Score: " + (int)gameScore;
        highScoreText.text = "High: " + (int)highScore;

        if(playerObject == null && !isGameOver)
        {
            GameOver();
        }
    }

    void FixedUpdate()
    {
        
        if (!isGameOver)
        {
            gameScore +=  1 * Time.deltaTime;
        }
    }

    bool IsFirstPlay()
    {
        if (PlayerPrefs.HasKey("IsFirstPlay"))
        {
            // is first play
            // set value to 1 so it has a key.
            PlayerPrefs.SetInt("IsFirstPlay", 1);
            return true;
        }
        else
        {
            // Not IsFirstPlay             
            return false;
        }        
    }


    public void GameOver()
    {
        isGameOver = true;        

        SetGameOverPlayerPrefs();
        RestartGame();
    }

    public void SetGameOverPlayerPrefs()
    {
        if (gameScore > highScore)
        {
            highScore = (int) gameScore;
            PlayerPrefs.SetInt("HighScore", highScore);
        }


        PlayerPrefs.Save();
    }



    // Button Functions
    public void RestartGame()
    {
        SceneManager.LoadScene ("Game");
    }

    public void ResetGame()
    {
        SceneManager.LoadScene("Menu");
    }

    public void PauseButton()
    {
        if (isPaused)
        {
            // UnPause
            isPaused = false;
            Time.timeScale = 1;
            menuButtons.SetActive(false);
        }
        else
        {
            // Pause
            isPaused = true;
            Time.timeScale = 0;
            menuButtons.SetActive(true);
        }
    }
}
