﻿using UnityEngine;
using System.Collections;

public class BallThrower : MonoBehaviour
{
    public GameObject dodgeball;

    public GameObject[] ballThrowPositions;

    private bool isColliderOn = true;

    private int posIndex = 0;

    private float xForce = -85;
    private float yForce = 0;

	// Use this for initialization
	void Start ()
    {
        InvokeRepeating("SpawnAndThrow", 1, 0.75f);
	}
	
	// Update is called once per frame
	void Update ()
    {  }

    void SpawnAndThrow()
    {
        // make this more linear! and cool! ya jackass!
        // maybe some easter eggs in here!
        SetBallThrowPosition();
        

        float x = Random.Range(xForce, xForce);
        float y = Random.Range(yForce, yForce);
        float forceMultiplier = Random.Range(12f, 15f);

        Vector2 force = new Vector2(x, y);
        GameObject clone = (GameObject)Instantiate(dodgeball, ballThrowPositions[posIndex].transform.position, Quaternion.identity);

        clone.GetComponent<Rigidbody2D>().AddForce(force * forceMultiplier, ForceMode2D.Force);
        StartCoroutine(WaitToDisable(clone));        
    }

    IEnumerator WaitToDisable(GameObject clone)
    {
        yield return new WaitForSeconds(5);
        isColliderOn = false;
        UpdateBallCollision(clone);
    }

    void UpdateBallCollision(GameObject clone)
    {
        if (clone != null && !isColliderOn && clone.transform.position.y > -10.27)
        {
            clone.gameObject.layer = 12;
        }        
    }

    void SetBallThrowPosition()
    {
        posIndex = Random.Range(0, 5);

        if(posIndex <= 1)
        {
            posIndex = 2;
        }
        else if(posIndex <= 3)
        {
            posIndex = 1;
        }
        else if(posIndex <= 4)
        {
            posIndex = 0;
        }
    }
}
