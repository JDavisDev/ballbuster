﻿using UnityEngine;
using System.Collections;

public class CloudSpawner : MonoBehaviour
{
    public GameObject[] cloudSpawners;
    public GameObject[] clouds;
    
    private float cloudSpawnRate = 5.5f;
	// Use this for initialization
	void Start ()
    {
        InvokeRepeating("SpawnClouds", 1.0f, cloudSpawnRate);
    }
	
	// Update is called once per frame
	void Update ()
    {
	    
	}

    void SpawnClouds()
    {
        int randClouds = Random.Range(0, clouds.Length);
        int randSpawner = Random.Range(0, cloudSpawners.Length);
        GameObject clone = (GameObject)Instantiate(clouds[randClouds], cloudSpawners[randSpawner].transform.position, Quaternion.identity);
    }
}
