﻿using UnityEngine;
using System.Collections;

public class Cloud : MonoBehaviour
{
    public GameObject[] cloudDeaths;
    public float moveSpeed = 2.5f;

    private int randCloudNum;

	// Use this for initialization
	void Start ()
    {
        cloudDeaths.SetValue(GameObject.Find("cloudDeath1"), 0);
        cloudDeaths.SetValue(GameObject.Find("cloudDeath2"), 1);
        this.randCloudNum = Random.Range(0, 2);
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (cloudDeaths[randCloudNum] != null)
        {
            transform.position = Vector2.MoveTowards(new Vector2(transform.position.x, transform.position.y),
                                       cloudDeaths[randCloudNum].transform.position,
                                       moveSpeed * Time.deltaTime);
        }
    }
}
