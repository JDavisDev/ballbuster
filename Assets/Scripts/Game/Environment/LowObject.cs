﻿using UnityEngine;
using System.Collections;

public class LowObject : MonoBehaviour
{
    private GameObject lowObjectKill;
    private static float moveSpeed;

    // Use this for initialization
    void Start ()
    {
        lowObjectKill = GameObject.Find("lowObjectKill");
        moveSpeed =4.85f;
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (lowObjectKill != null)
        {
            transform.position = Vector2.MoveTowards(new Vector2(transform.position.x, transform.position.y),
                                       lowObjectKill.transform.position, 
                                       moveSpeed * Time.deltaTime);
        }
    }

    public static float objectSpeed
    {
        get
        {
            return moveSpeed;
        }
        set
        {
            moveSpeed = value;
        }
    }

}
