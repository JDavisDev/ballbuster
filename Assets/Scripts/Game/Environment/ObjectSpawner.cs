﻿using UnityEngine;
using System.Collections;

public class ObjectSpawner : MonoBehaviour
{

    public GameObject[] lowObjects;
    public GameObject[] midObjects;
    public GameObject[] highObjects;
    public GameObject lowSpawnLocation;
    public GameObject midSpawnLocation;
    public GameObject highSpawnLocation;
    public GameObject lowSpawnKill, midSpawnKill, highSpawnKill;


    private float obstacleSpawnRate = 7.0f;

	// Use this for initialization
	void Start ()
    {
        InvokeRepeating("SpawnObstacles", 5.0f, obstacleSpawnRate);
        
    }
	
	// Update is called once per frame
	void Update ()
    {
        
	}

    void SpawnObstacles()
    {
        int randHeight = Random.Range(0, 4);
        if (randHeight == 0 || randHeight == 1)
        {
            SpawnLowObstacle();
        }
        else if (randHeight == 2)
        {
            SpawnMidObstacle();
        }
        else if(randHeight == 3)
        {
            SpawnHighObstacle();
        }
    }

    void SpawnLowObstacle()
    {
        int randObject = Random.Range(0, lowObjects.Length);
        GameObject clone = (GameObject)Instantiate(lowObjects[randObject], lowSpawnLocation.transform.position, Quaternion.identity);        
    }

    void SpawnMidObstacle()
    {
        int randObject = Random.Range(0, midObjects.Length);
        GameObject clone = (GameObject)Instantiate(midObjects[randObject], midSpawnLocation.transform.position, Quaternion.identity);
    }

    void SpawnHighObstacle()
    {
        int randObject = Random.Range(0, highObjects.Length);
        GameObject clone = (GameObject)Instantiate(highObjects[randObject], highSpawnLocation.transform.position, Quaternion.identity);
    }
}
