﻿using UnityEngine;
using System.Collections;

public class HighObject : MonoBehaviour
{
    private GameObject highObjectKill;
    private static float moveSpeed;

    // Use this for initialization
    void Start()
    {
        highObjectKill = GameObject.Find("highObjectKill");
        moveSpeed = 5f;
    }

    // Update is called once per frame
    void Update()
    {
        if (highObjectKill != null)
        {
            transform.position = Vector2.MoveTowards(new Vector2(transform.position.x, transform.position.y),
                                       highObjectKill.transform.position,
                                       moveSpeed * Time.deltaTime);
        }
    }

    public static float objectSpeed
    {
        get
        {
            return moveSpeed;
        }
        set
        {
            moveSpeed = value;
        }
    }


}
