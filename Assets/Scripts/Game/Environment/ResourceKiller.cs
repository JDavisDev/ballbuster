﻿using UnityEngine;
using System.Collections;

public class ResourceKiller : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    // This represents the collider box under the level. 
    // collects falling objects, namely balls.
    public void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Player"      ||
            collision.gameObject.tag == "Low Object" ||
            collision.gameObject.tag == "Mid Object" || 
            collision.gameObject.tag == "Cloud"      ||
            collision.gameObject.tag == "Dodgeball"  ||
            collision.gameObject.tag == "High Object")
        {
            Destroy(collision.gameObject);
        }
    }

    
}
