﻿using UnityEngine;
using System.Collections;

public class ScrollingTexture : MonoBehaviour
{
    
    public  float scrollSpeed = 0.39f;
    public Renderer rend;

    void Start()
    {
        rend = GetComponent<Renderer>();
    }

    void Update()
    {
        float offset = Time.time * scrollSpeed;
        rend.material.SetTextureOffset("_MainTex", new Vector2(offset, 0));
    }

    public float floorScrollSpeed
    {
        get
        {
            return scrollSpeed;
        }
        set
        {
            scrollSpeed = value;
        }
    }
}
