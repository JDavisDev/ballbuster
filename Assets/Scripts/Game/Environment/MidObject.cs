﻿using UnityEngine;
using System.Collections;

public class MidObject : MonoBehaviour
{
    private GameObject midObjectKill;
    private static float moveSpeed;

    // Use this for initialization
    void Start()
    {
        midObjectKill = GameObject.Find("midObjectKill");
        moveSpeed = 5f;
    }

    // Update is called once per frame
    void Update()
    {
        if (midObjectKill != null)
        {
            transform.position = Vector2.MoveTowards(new Vector2(transform.position.x, transform.position.y),
                                       midObjectKill.transform.position,
                                       moveSpeed * Time.deltaTime);
        }
    }

    public static float objectSpeed
    {
        get
        {
            return moveSpeed;
        }
        set
        {
            moveSpeed = value;
        }
    }


}
