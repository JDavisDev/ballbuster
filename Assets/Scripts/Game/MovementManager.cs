﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MovementManager : MonoBehaviour
{
    private const float ROLLTHRESHOLDTIME = 0.2f;
    public static bool onGround;

    public Animator animator;
    public Rigidbody2D playerRB;

    private ScrollingTexture scrollTexture;
    private GameManager gameManager;
    private Vector2 startPos;
    private GameObject[] activeLowObjects;
    private GameObject[] activeMidObjects;
    private GameObject[] activeHighObjects;
    private LowObject lowObject;
    
    private int jumpCount;
    private float touchScreenTime;
    
    private bool isJumping;

    // Use this for initialization
    void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
        startPos = playerRB.position;

        if (PlayerPrefs.HasKey("FirstPlay"))
        {
            // first load, load up tutorial stuff.
            // move this to a GameManager script.
        }
    }


    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount > 0)
        {
            PlayerMovement();
        }
        else if (transform.position.x > startPos.x && !isJumping)
        {
            MovePlayerBack();
        }
    }

    void PlayerMovement()
    {
        Touch touchOne = Input.GetTouch(0);
        Touch touchTwo = new Touch();

        // second finger used
        if(Input.touchCount > 1)
        {
            touchTwo = Input.touches[1];
        }

        // jump with tap on left side
        if (touchOne.phase == TouchPhase.Began && touchOne.position.x <= Screen.width / 2)
        {
            // left side
            PlayerJump();
        }
        // roll if on right side and one finger
        else if (touchOne.position.x > Screen.width / 2 && 
            touchOne.phase == TouchPhase.Stationary || 
            touchOne.phase == TouchPhase.Moved || 
            touchOne.phase == TouchPhase.Began) 
        {
            // right side
            PlayerRoll();
        }
        // neither running nor jumping
        else
        {
            animator.SetBool("isRolling", false);
            GetComponent<BoxCollider2D>().size = new Vector2(1.8989f, 4.909f);
        }

        // override roll and jump!
        // user has two fingers on and one on left side.
        if (Input.touchCount > 1 && touchTwo.phase == TouchPhase.Began && touchTwo.position.x <= Screen.width / 2)
        {
            // left side with finger two!
            PlayerJump();
        }
    }

    public void PlayerJump()
    {
        if (jumpCount < 2)
        {
            jumpCount++;
            isJumping = true;
            onGround = false;
            Vector2 jumpForce = new Vector2(0.0f, 4300f);
            if(animator.GetBool("isRolling") == true)
            {
                animator.SetBool("isRolling", false);
            }
            
            animator.SetBool("isJumping", true);
            playerRB.AddForce(jumpForce);
            GetComponent<BoxCollider2D>().size = new Vector2(2.691f, 3.98f);
            GetComponent<BoxCollider2D>().offset = new Vector2(0.025f, 0.1202f);
        }
    }

    public void PlayerRoll()
    {
        // shrink player collider | use animation
        if (!isJumping && Input.touchCount > 0)
        {
            MovePlayerForward();
            animator.SetBool("isRolling", true);
            GetComponent<BoxCollider2D>().size = new Vector2(2.468f, 2.642f);
            GetComponent<BoxCollider2D>().offset = new Vector2(-0.0473f, -0.0485f);
        }
    }

    void MovePlayerBack()
    {
        transform.position = Vector2.MoveTowards(transform.position, startPos, 4 * Time.deltaTime);
    }


    void MovePlayerForward()
    {
        transform.position = Vector2.MoveTowards(transform.position, new Vector2(7.73f, transform.position.y), 5 * Time.deltaTime);
    }

    private void GameOver()
    {
        if (gameManager != null)
        {
            gameManager.GameOver();
        }
    }

    // Getting hit with dodgeball
    public void OnCollisionEnter2D(Collision2D collision)
    {
        string tag = collision.gameObject.tag;

        if (tag != null && tag != "Floor" && tag == "Dodgeball")
        {
            GameOver();
        }
    }

    // check if we are on the ground
    // reset jump count
    void OnCollisionStay2D(Collision2D col)
    {
        if (col.gameObject.tag == "Floor")
        {
            onGround = true;
            isJumping = false;
            jumpCount = 0;
            animator.SetBool("isJumping", false);
        }
    }
}
