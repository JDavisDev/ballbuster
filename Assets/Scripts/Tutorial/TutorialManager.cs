﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TutorialManager : MonoBehaviour
{
    private int ballLayer = 1 << 10;
    private int playerLayer = 1 << 8;
    public Image tapImage;
    public Image rollImage;
    public Image dodgeballImage;
    public Image obstacleImage;
    public Image leftScreen;
    public Image rightScreen;
    public Text tapText;
    public Text rollText;
    public Text dodgeText;

    public Button backToGameBtn;

    public GameObject ballThrower;
    public GameObject menuButtons;
    public GameObject player;

    public Animator animator;

    public Rigidbody2D playerRB;

    private int jumpCount = 0;
    private bool isJumping = false;

	// Use this for initialization
	void Start ()
    {
	}
	
	// Update is called once per frame
	void Update ()
    {
	    if(MovementManager.onGround)
        {
            jumpCount = 0;
        }
	}

    public void StartTutorial()
    {
        // show all the images.. maybe a sequence or animation or static stuffs...
        // I could just start and stop specific animations one after another.. yeah...
        Time.timeScale = 1;
        ToggleAllUIElements(true);
        menuButtons.SetActive(false);
        player.GetComponent<BoxCollider2D>().enabled = true;
        player.layer = LayerMask.NameToLayer("ball");
    }

    public void EndTutorial()
    {
        ToggleAllUIElements(false);
        Time.timeScale = 0;
        player.GetComponent<BoxCollider2D>().enabled = true;
        player.layer = LayerMask.NameToLayer("player");
    }

    private void ToggleAllUIElements(bool value)
    {
        leftScreen.gameObject.SetActive(value);
        rightScreen.gameObject.SetActive(value);
        ballThrower.gameObject.SetActive(!value);
        backToGameBtn.gameObject.SetActive(value);
        /*
        tapImage.gameObject.SetActive(value);
        dodgeballImage.gameObject.SetActive(value);
        tapText.gameObject.SetActive(value);
        rollText.gameObject.SetActive(value);
        dodgeText.gameObject.SetActive(value);
        */
    }

    



    /**********************
    *   Button Functions  *
    **********************/

    public void LeftScreenTap()
    {
        PlayerJump();
    }

    public void PlayerJump()
    {
        if (jumpCount < 2)
        {
            jumpCount++;
            isJumping = true;
            Vector2 jumpForce = new Vector2(0.0f, 4300f);
            if (animator.GetBool("isRolling") == true)
            {
                animator.SetBool("isRolling", false);
            }

            animator.SetBool("isJumping", true);
            playerRB.AddForce(jumpForce);            
        }
    }

    public void RightScreenTap()
    {

    }

    public void BackToGameClick()
    {
        EndTutorial();
    }
}
